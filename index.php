<?php

// On force la déclaration des types des variables
declare(strict_types = 1);

// region DefConst : définition des informations contantes
// Le tableau ChiFouMi représente les choix possibles et quel choix gagne contre quel choix
const ChiFouMi = [
    "Pierre"  => "Ciseaux",
    "Ciseaux" => "Papier",
    "Papier"  => "Pierre" ];
// Conversion du choix du joueur humain en une valeur sous forme de chaîne de caractère
const ValChoix = [
    "1" => "Pierre",
    "2" => "Ciseaux",
    "3" => "Papier" ];
// endregion DefConst
// region DefVars : définition des variables
// Le tableau $Joueurs stocke les informations sur les 2 joueurs
( array ) $Joueurs     = [
        ];
// Le tableau $InfosJoueur stocke les données d'un seul joueur et est utilisé pour remplir $Joueurs
( array ) $InfosJoueur = [
    "Nom"    => "",
    "Points" => 0 ];
// endregion DefVars
// Saisie du nom du joueur tant que la réponse est vide
while( !$InfosJoueur["Nom"] ):  // Pour PHP, la chaîne vide "" est équivalente à FALSE
    // Prompt
    print( 'Nom du joueur ? ' );
    // Récupération de la réponse. Notez que la fonction PHP readline() ne fonctionne pas sous Windows.
    $InfosJoueur["Nom"] = trim( fgets( STDIN ) );
endwhile;
// Valider la saisie de la réponse en l'affichant dans un message de bienvenue.
printf( 'Bonjour, %s.' . PHP_EOL, $InfosJoueur["Nom"] );
// AJout des infos sur le joueur : son nom et son score de départ
$Joueurs[]          = $InfosJoueur;
// Le 2ème joueur est joué par l'ordinateur et s'appelle "Ordinateur"
$InfosJoueur["Nom"] = "Ordinateur";
$Joueurs[]          = $InfosJoueur;
// Début de la partie en 3 manches gagnantes
while( $Joueurs[0]["Points"] < 3 && $Joueurs[1]["Points"] < 3 ) :
    // Annonce de la nouvelle manche
    printf( 'Nouvelle manche !' . PHP_EOL );
//    echo PHP_EOL;
    // Rappel des scores de chaque joueur
    printf( 'Rappel des scores : %s %d point(s). Ordinateur %d point(s).' . PHP_EOL,
            $Joueurs[0]["Nom"], $Joueurs[0]["Points"], $Joueurs[1]["Points"] );
    // Initialisation du choix du joueur à une valeur hors limite
    ( int ) $ChoixJoueur = 0;
    // Saisie du choix du Joueur humain
    while( $ChoixJoueur < 1 || $ChoixJoueur > 3 ):
        // Prompt
        print( 'Que jouez-vous [Répondre 1, 2 ou 3] ? (1 = ' . ValChoix["1"] . ', 2 = ' . ValChoix["2"] . ', 3 = ' . ValChoix["3"] . ') ' );
        // Récupération de la réponse. Notez que la fonction PHP readline() ne fonctionne pas sous Windows.
        $ChoixJoueur = intval( trim( fgets( STDIN ) ) );
    endwhile;
    // Confirmation du choix du joueur en l'affichant en toute lettre
    printf( 'Vous avez choisi : %s', ValChoix[strval( $ChoixJoueur )] . PHP_EOL );
    // Tirage aléatoire du choix du Joueur "Ordinateur"
    ( string ) $ChoixOrdinateur = ValChoix[array_rand( ValChoix )];
    // Confirmation du choix de l'ordinateur en l'affichant en toute lettre
    printf( 'L\'ordinateur a choisi : %s' . PHP_EOL, $ChoixOrdinateur );
    // Résolution de la manche
    If( ValChoix[strval( $ChoixJoueur )] != $ChoixOrdinateur ):
        if( ChiFouMi[ValChoix[strval( $ChoixJoueur )]] == $ChoixOrdinateur ):
            // Le joueur huemain gagne la manche
            printf( '%s gagne la manche !' . PHP_EOL, $Joueurs[0]["Nom"] );
            $Joueurs[0]["Points"]++;
        else:
            // L'ordinateur gagne la manche
            printf( 'L\'ordinateur gagne la manche !' . PHP_EOL );
            $Joueurs[1]["Points"]++;
        endif;
    else :
        printf( 'Match nul !' . PHP_EOL );
    endif;
endwhile;
// Afficher le résultat de la partie
if( 3 == $Joueurs[0]["Points"] ):
    printf( '%s gagne la partie !' . PHP_EOL, $Joueurs[0]["Nom"] );
else:
    printf( 'L\'ordinateur gagne la partie !' . PHP_EOL );
endif;